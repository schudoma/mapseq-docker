FROM ubuntu:22.04

LABEL maintainer="cschu1981@gmail.com"
LABEL version="0.1"
LABEL description="MAPseq"

ENV LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/local/lib

ARG DEBIAN_FRONTEND=noninteractive

RUN apt update
RUN apt upgrade -y

RUN apt install -y wget python3-pip git dirmngr gnupg ca-certificates build-essential libssl-dev libcurl4-gnutls-dev libxml2-dev libfontconfig1-dev libharfbuzz-dev libfribidi-dev libfreetype6-dev libpng-dev libtiff5-dev libjpeg-dev subversion autotools-dev autoconf libtool libncurses5-dev
RUN apt clean


ARG CACHEBUST=1
RUN mkdir -p /opt/software && cd /opt/software && git clone --single-branch -b devel https://github.com/jfmrod/MAPseq.git
WORKDIR /opt/software/MAPseq

RUN svn co https://www.konceptfx.com/svn/eutils

# RUN sed -i '3d; 4d' setup.sh && cat setup.sh
# RUN git checkout devel


# RUN ./setup.sh 
RUN sed -i 1233d eutils/eparser.cpp
RUN sed -i "s:libs/eutils:eutils:" Makefile.am configure.ac && sed -i 38d Makefile.am
RUN sed -i '9i #include <cmath>' eseqdb.cpp

# RUN sed -i '72i seed=313;' eutils/ernd.cpp && grep "seed=" eutils/ernd.cpp


RUN ./bootstrap && ./configure

RUN sed -i "s/^DATA = .\+/DATA = /" Makefile
RUN cat Makefile

# DATA = $(mapref_DATA)


#RUN sed -i 's/epregisterFunc(\(.\+\))/epregisterFunc3(\1, "\1")/; s/evvararray/evararray/; s/results.add(tmparr,func.call(tmparr))/results.add(func.call#(tmparr))/; ' libs/eutils/eparser.cpp
#sed -i 's/evvararray/evararray/' libs/eutils/eparser.cpp && \
#sed -i 's/results.add(tmparr,func.call(tmparr))/results.add(func.call(tmparr))/' libs/eutils/eparser.cpp && \
#RUN sed -i 's/evar apply/evararray apply/' libs/eutils/eparser.cpp libs/eutils/eparser.h 

#RUN grep apply libs/eutils/eparser.cpp libs/eutils/eparser.h


# downloads eutils and the reference data files
# RUN cd /opt/software/MAPseq && chmod 755 setup.sh bootstrap configure.ac && ./setup.sh
# this step is only needed if you cloned the repository, you will also need to install autotools/autoconf packages
# RUN cd /opt/software/MAPseq && ./bootstrap
# RUN cd /opt/software/MAPseq && ./configure

# RUN cd /opt/software/MAPseq && sed -i 's/epregisterFunc(\(.\+\))/epregisterFunc3(\1, "\1")/' libs/eutils/eparser.cpp
# RUN grep apply /opt/software/MAPseq/libs/eutils/eparser.cpp
# RUN cd /opt/software/MAPseq && sed -i 's/evvararray/evararray/' libs/eutils/eparser.cpp
# epregisterFunc(apply);

# results.add(tmparr,func.call(tmparr));

RUN make
RUN make install

RUN ln -s /usr/local/lib/libeutils-1.0.so /usr/lib/libeutils-1.0.so

